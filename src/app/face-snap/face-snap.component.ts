import { FaceSnap } from './../models/face-snap.models';
import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-face-snap',
    templateUrl: './face-snap.component.html',
    styleUrls: ['./face-snap.component.scss']
})
export class FaceSnapComponent implements OnInit {
    @Input() faceSnap!: FaceSnap;
    buttonText!: string;

    ngOnInit(): void {
        this.buttonText = "Oh Snap!"
    }

    onSnap(){
        if (this.buttonText === "On Snap!") {
            this.faceSnap.snaps ++;
            this.buttonText = "Oops, unSnap!";
        }else{
            this.faceSnap.snaps --;
            this.buttonText = "On Snap!";
        }
    }
}
